﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BasicDI.UnitTests
{
    public class Node
    {
        public Type Value { get; private set; }

        public List<Node> Children { get; private set; } = new List<Node>();

        public Node(Type value)
        {
            this.Value = value;
        }
    }

    public class Graph
    {
        private readonly List<Node> nodeList;

        public Graph()
        {
            this.nodeList = new List<Node>();
        }

        public int GetNodeCount()
        {
            return this.nodeList.Count();
        }

        public void AddNode(Type value)
        {
            this.nodeList.Add(new Node(value));
        }

        public void AddDependency(Node from, Node to)
        {
            from.Children.Add(to);

            if (to.Children.Any())
            {
                Traverse(to, to);
            }
        }

        /// <summary>
        /// Are any children dependent on a node that comes previously in the chain (a circular dependency)
        /// </summary>
        /// <param name="node"></param>
        private void Traverse(Node node, Node check)
        {
            foreach (var child in node.Children)
            {
                if (child.Value == check.Value)
                {
                    throw new CircularDependencyException();
                }

                Traverse(child, check);
            }
        }

        public Node GetNode(Type type)
        {
            return nodeList.Where(n => n.Value == type).SingleOrDefault();
        }
    }
}