using System;
using System.Collections;
using Xunit;

using System.Linq.Expressions;
using System.Linq;
using System.Collections.Generic;

namespace BasicDI.UnitTests
{
    public class RegistrationTests
    {
        [Fact]
        public void Should_register_a_concrete_type_with_no_dependencies()
        {
            var sut = new Container();
            sut.Register<TestType>();

            var result = sut.Registrations();
            Assert.Equal(1, result);
        }

        // ok
        [Fact]
        public void Should_create_instance_of_concrete_type_with_no_dependencies()
        {
            var sut = new Container();
            sut.Register<TestType>();

            var instance = sut.GetInstance<TestType>();

            Assert.NotNull(instance);
            Assert.IsType<TestType>(instance);
            Assert.Equal("Hello, world!", instance.GetHelloMsg());
        }

        // OK
        [Fact]
        public void Should_throw_not_registered_exception_when_trying_to_create_unregistered_type()
        {
            var sut = new Container();

            Assert.Throws<UnregisteredTypeException>(()
                => sut.GetInstance<TestType>());
        }

        // ok
        [Fact]
        public void Should_register_concrete_type_with_concrete_dependency()
        {
            var sut = new Container();
            sut.Register<TestType>();

            sut.Register<TestTypeWithDependency>();

            var result = sut.Registrations();
            Assert.Equal(2, result);
        }

        // ok
        [Fact]
        public void Should_throw_unregistered_dependency_exception_when_creating_instance_with_unregistered_dependency()
        {
            var sut = new Container();
            sut.Register<TestTypeWithDependency>();

            Assert.Throws<UnregisteredTypeException>(()
               => sut.GetInstance<TestTypeWithDependency>());
        }

        // TODO: Already Registered

        // TODO: Circular Dependency

        [Fact]
        public void Should_create_instance_of_concrete_type_with_concrete_dependency()
        {
            var sut = new Container();
            sut.Register<HelloService>();
            sut.Register<TestTypeWithDependency>();

            var instance = sut.GetInstance<TestTypeWithDependency>();

            Assert.NotNull(instance);
            Assert.IsType<TestTypeWithDependency>(instance);
        }

        [Fact]
        public void Should_create_instance_of_concrete_type_with_multiple_concrete_dependencies()
        {
            var sut = new Container();
            sut.Register<LevelA>();
            sut.Register<LevelB>();
            sut.Register<LevelC>();

            var instance = sut.GetInstance<LevelA>();

            Assert.NotNull(instance);
            Assert.IsType<LevelA>(instance);
        }

        [Fact]
        public void Should_throw_exception_when_has_circular_dependency()
        {
            var sut = new Container();
            sut.Register<TestClassD>();
            sut.Register<TestClassE>();
            sut.Register<TestClassF>();

            Assert.Throws<CircularDependencyException>(()
              => sut.GetInstance<TestClassF>());
        }
    }

    public class Container
    {
        private readonly Graph dependencyGraph;

        public Container()
        {
            dependencyGraph = new Graph();
        }

        public void Register<T>()
        {
            var type = typeof(T);

            dependencyGraph.AddNode(type);
        }

        public T GetInstance<T>()
            where T : class
        {
            var registration = dependencyGraph.GetNode(typeof(T));

            if (registration == null)
            {
                throw new UnregisteredTypeException();
            }

            return InstantiateDependencyGraph(registration.Value) as T;
        }

        private object InstantiateDependencyGraph(Type registration)
        {
            var constructors = registration.GetConstructors();
            var parameters = constructors.SelectMany(c => c.GetParameters());
            var dependencies = parameters.Select(p => p.ParameterType);
            var instantiatedDependencies = new List<object>();

            foreach (var dependency in dependencies)
            {
                var child = dependencyGraph.GetNode(dependency);

                if (child == null)
                {
                    throw new UnregisteredTypeException();
                }
                else
                {
                    var parent = dependencyGraph.GetNode(registration);
                    dependencyGraph.AddDependency(parent, child);

                    var instantiatedDependency = InstantiateDependencyGraph(dependency);
                    instantiatedDependencies.Add(instantiatedDependency);
                }
            }

            return Activator.CreateInstance(registration, instantiatedDependencies.ToArray());
        }

        public int Registrations()
        {
            return dependencyGraph.GetNodeCount();
        }
    }

    public class UnregisteredTypeException : Exception
    {
    }

    public class CircularDependencyException : Exception
    {
    }

    public class TestType
    {
        public string GetHelloMsg()
        {
            return "Hello, world!";
        }
    }

    public class TestTypeWithDependency
    {
        private readonly HelloService helloService;

        public TestTypeWithDependency(HelloService service)
        {
            this.helloService = service;
        }

        public string Hello()
        {
            return this.helloService.GetHelloMsg();
        }
    }

    public class HelloService
    {
        public string GetHelloMsg()
        {
            return "Hello, world!";
        }
    }

    public class LevelA
    {
        private readonly LevelB b;
        private readonly LevelC c;

        public LevelA(LevelB b, LevelC c)
        {
            this.b = b;
            this.c = c;
        }

        public void Hi()
        {
            this.b.Hi();
            this.c.Hi();
        }
    }

    public class LevelB
    {
        private readonly LevelC c;

        public LevelB(LevelC c)
        {
            this.c = c;
        }

        public void Hi()
        {
            this.c.Hi();
        }
    }

    public class LevelC
    {
        public void Hi()
        {
        }
    }

    public class TestClassD
    {
        public TestClassD(TestClassE e)
        {
        }
    }

    public class TestClassE
    {
        public TestClassE(TestClassF f)
        {
        }
    }

    public class TestClassF
    {
        public TestClassF(TestClassD d)
        {
        }
    }
}